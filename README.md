# Intall

## Ubuntu

sudo apt install \
	vim \
	tmux \
	silversearch-ag \
	make \
	docker-compose \
	ipcalc \
	source-highlight

## VIM

```
$ git clone https://github.com/VundleVim/Vundle.vim.git ~/workspace/dotfiles/vim/bundle/Vundle.vim
$ ln -s ~/workspace/dotfiles/vimrc ~/.vimrc
$ ln -s ~/workspace/dotfiles/vim ~/.vim
```

And in vim
```
:PlugInstall
:PluginInstall

## Bash

ln -s ~/workspace/dotfiles/bashrc ~/.bashrc

## Kube

git clone https://github.com/ahmetb/kubectx ~/.local/kubectx
ln -s ~/.local/kubectx/kubectx ~/.local/bin/kubectx
ln -s ~/.local/kubectx/kubens ~/.local/bin/kubens

## Tmux

ln -s workspace/dotfiles/tmux.conf ~/tmux.conf

# Links

https://github.com/equalsraf/win32yank/releases
