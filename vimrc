" ------------------------------------- PLUG ------------------------------------------
call plug#begin('~/.vim/plugged')

Plug 'govim/govim'
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'tmux-plugins/vim-tmux'
Plug 'vimwiki/vimwiki'

call plug#end()

" ------------------------------------- VUNDLE ------------------------------------------
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'hashivim/vim-terraform'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'lifepillar/vim-solarized8'
Plugin 'pearofducks/ansible-vim'
Plugin 'preservim/nerdcommenter'

call vundle#end()            " required
filetype plugin on
filetype plugin indent on    " required

" ------------------------------------- General ------------------------------------------

" Default settings
set noexpandtab

" Why do they fail with clipboards so much? (This works with Windows10 + WSL2)
vmap <C-y> :w !win32yank.exe -i<CR><CR>
map <C-p> :r!win32yank.exe -o<CR>

" Copy paste to / from PRIMARY clipboard (This does not work with Windows10 + WSL2)
noremap <Leader>y "*y
noremap <Leader>p "*p

" Copy paste to / from CLIPBOARD clipboard (This does not work with Windows 10 + WSL2)
noremap <Leader>Y "+y
noremap <Leader>P "+p

" Colors!!!
if (has("termguicolors"))
  set termguicolors
endif

" Change window with alt+arrow
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

" Nowrapety wrap!
"set nowrap

" Let there be mouse selecting
set mouse=a

" Better autocomplete for files
set wildmenu
set wildmode=list:longest,full

" Ctrl+s for save
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a


" Do not auto-wrap text using textwidth(https://vimhelp.org/change.txt.html#fo-table
autocmd FileType * set formatoptions-=c

" Do not auto-wrap comments using textwidth(https://vimhelp.org/change.txt.html#fo-table)
autocmd FileType * set formatoptions-=t

" no expand tab
set noet

" Make uuid
noremap <c-u> :r !uuidgen<CR>

" Show tabs and spaces
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣
noremap <F5> :set list!<CR>
"inoremap <F5> <C-o>:set list!<CR>
"cnoremap <F5> <C-c>:set list!<CR>

" no need to syntax highligh long rows
set synmaxcol=512

set background=dark
colorscheme solarized8_flat

" File specific rules
set modeline
set modelines=5

" Set true colors
if exists('+termguicolors')
        let &t_8f="\<Esc>[38;2;%lu;%lu;%lum"
        let &t_8b="\<Esc>[48;2;%lu;%lu;%lum"
        set termguicolors
endif

" ------------------------------------- Nerd Comment ------------------------------------------
let g:NERDCommentEmptyLines = 1
let g:NERDCompactSexyComs = 0
let g:NERDCustomDelimiters = {'text':{'left':'#','right':''}}
let g:NERDSpaceDelims = 1
let g:NERDToggleCheckAllLines = 0

" ------------------------------------- Nerd tree ------------------------------------------

" Nerd tree configs
map <C-n> :NERDTreeToggle<CR>


" ------------------------------------- Whitespace ------------------------------------------
"  No highlight overriding please
let g:better_whitespace_ctermcolor='none'
let g:better_whitespace_guicolor='NONE'

" ------------------------------------- VIM Wiki ------------------------------------------
" Markdown is better
let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]

