# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !

# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Put your fun stuff here.

if [ -f /etc/gentoo-release ]; then
	alias updateWorld="sudo emerge --ask --deep --newuse --update --changed-use --changed-deps --verbose-conflicts @world"
fi

hostname=`hostname`
minikubeCpu=2
minikubeMemory=4096
minikubeDisk="20000mb"
minikubeKubernetes="1.20.4"
if [ $hostname == "huisku" ]; then
	minikubeMemory="32768mb"
	minikubeCpu="8"
	minikubeDisk="80000mb"
elif [ $hostname == "jani-asus" ]; then
	minikubeMemory="6134mb"
	minikubeCpu="4"
	minikubeDisk="80000mb"
fi

alias myip4='curl -4 ifconfig.io'
alias myip6='curl -6 ifconfig.io'
alias weather='curl wttr.in/Oulu'
alias virsh='virsh --connect qemu:///system '

# Get rid of ctrl+s / ctrl+q feature
stty -ixon


# Instead rewriting history file per session, we want to append there
shopt -s histappend
# Store multiline commands to single entry
shopt -s cmdhist
# No duplicate lines to history and ignore the ones starting with space. Also remove duplicates if some exists
export HISTCONTROL=ignoredups:ignorespace:erasedups
# History size
export HISTSIZE=10000
export HISTFILESIZE=20000
# Append to history and reload in case other terminal has new stuff
export PROMPT_COMMAND='history -a;history -r'
export GPG_TTY=$(tty)

# ~/.local/bin
if [ -d $HOME/.local/bin ]; then
	PATH="$HOME/.local/bin:$PATH"
fi

# ~/.krew/bin
if [ -d $HOME/.krew/bin ]; then
	PATH="$HOME/.krew/bin:$PATH"
fi

# ~/.local/go/bin
if [ -d $HOME/.local/go/bin ]; then
	PATH="$HOME/.local/go/bin:$PATH"
fi

# ~/go/bin
if [ -d $HOME/go/bin ]; then
	PATH="$PATH:$HOME/go/bin"
fi

# ~/node/bin
if [ -d $HOME/.local/node/bin ]; then
	PATH="$HOME/.local/node/bin:$PATH"
fi

# ~/.local/pact
if [ -d $HOME/.local/pact ]; then
	PATH="$PATH:$HOME/.local/pact/bin"
fi

if [ -x /usr/bin/ag ]; then
	alias ag="/usr/bin/ag --hidden --ignore .git --ignore node_modules"
fi

if hash tmux 2>/dev/null; then
	alias tmuxm="tmux -u new-session -A -s main"
	alias tmuj="tmux -u new-session -A -s jp"
	alias tmuxt="tmux -u new-session -A -s temp"
fi

if hash kubectl 2>/dev/null; then
	alias k="kubectl"
	if [ ! -f ~/.kube/completion.bash.inc ]; then
		kubectl completion bash > ~/.kube/completion.bash.inc
	fi
	source ~/.kube/completion.bash.inc
	complete -F __start_kubectl k
fi

if [ -f ~/.local/kubectx/completion/kubectx.bash ]; then
	source ~/.local/kubectx/completion/kubectx.bash
fi

if [ -f ~/.local/kubectx/completion/kubens.bash ]; then
	source ~/.local/kubectx/completion/kubens.bash
fi

if hash minikube 2>/dev/null; then
	alias minikubeStop="minikube stop"
	alias minikubeSet="eval \$(minikube docker-env)"
	alias minikubeUnset="unset DOCKER_TLS_VERIFY DOCKER_HOST DOCKER_CERT_PATH"
fi

if hash rsync 2>/dev/null; then
	function cpr() {
	  rsync --archive -hh --partial --info=stats1 --info=progress2 --modify-window=1 "$@"
	}
	function mvr() {
	  rsync --archive -hh --partial --info=stats1 --info=progress2 --modify-window=1 --remove-source-files "$@"
	}
fi

if hash go 2>/dev/null; then
	export GOPROXY="https://proxy.golang.org"
	export GONOSUMDB="gitlab.viidakko.fi,gitlab.com/johtopilvi"
	export GOPRIVATE="gitlab.viidakko.fi,gitlab.com/johtopilvi"
fi

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/jani/.local/google-cloud-sdk/path.bash.inc' ]; then . '/home/jani/.local/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/jani/.local/google-cloud-sdk/completion.bash.inc' ]; then . '/home/jani/.local/google-cloud-sdk/completion.bash.inc'; fi

function minikubeStart() {
	export MINIKUBE_IN_STYLE=false
	export MINIKUBE_WANTUPDATENOTIFICATION=false
	minikube start --memory=${minikubeMemory} --cpus=${minikubeCpu} --disk-size=${minikubeDisk} --kvm-network=minikube -v 4 --vm-driver kvm2 --kubernetes-version ${minikubeKubernetes} --bootstrapper kubeadm --addons metrics-server --addons dashboard --addons ingress --addons ingress-dns
	kubectl create secret generic gitlab-registry --from-file=.dockerconfigjson=${HOME}/.docker/config.json --type=kubernetes.io/dockerconfigjson

	if [ -f /etc/NetworkManager/dnsmasq.d/minikube.conf ]; then
		dnsmasqMinikubeData=$(sed "s/^server=\/\([a-z0-9.]*\)\/.*$/server=\/\1\/`minikube ip`/" /etc/NetworkManager/dnsmasq.d/minikube.conf)
		oldData=$(cat /etc/NetworkManager/dnsmasq.d/minikube.conf)
		if [ "$dnsmasqMinikubeData" != "$oldData" ]; then
			echo "Changing minikube ip"
			echo "$dnsmasqMinikubeData" | sudo tee /etc/NetworkManager/dnsmasq.d/minikube.conf
			sudo systemctl restart NetworkManager
		fi
	fi
}

function kubectlConfigs() {
	data=""
	for i in $HOME/.kube/config*; do # Whitespace-safe but not recursive.
		if [ ${#data} -gt 0 ]; then
			data="$data:$i"
		else
			data="$i"
		fi
	done
	export KUBECONFIG="$data"
}

kubectlConfigs

function c() {
	echo "$*" | bc -l
}

if [[ -e /run/user/$UID/keyring/control ]]; then
    eval $(/usr/bin/gnome-keyring-daemon --start --components=ssh)
    export SSH_AUTH_SOCK
fi

if [[ -e /usr/bin/keychain ]]; then
	keys=$(find ~/.ssh -name "*id*" | grep -v -e ".pub\$")
	eval `keychain --eval --agents gpg,ssh C5DDF28B8D1304B72DD5DDB608941E84738DC39A 83BF513B09D3FE45029F09F96F798D26646AE86B $keys`
fi

if [[ -e /usr/bin/vim ]]; then
	export EDITOR=vim
fi

if [[ -d /home/linuxbrew/.linuxbrew/bin ]]; then
	export PATH="$PATH:/home/linuxbrew/.linuxbrew/bin"
	eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
fi

if [[ -x /usr/share/source-highlight/src-hilite-lesspipe.sh ]]; then
	export LESSOPEN="| /usr/share/source-highlight/src-hilite-lesspipe.sh %s"
elif [[ -x /usr/bin/src/hilite-lesspipe.sh ]]; then
	export LESSOPEN="| /usr/bin/src-hilite-lesspipe.sh %s"
fi
export LESS=" -Rc"

genpasswd() {
        local l=$1
        [[ $l == *[[:digit:]]* ]] || l=32
        tr -dc A-Za-z0-9 < /dev/urandom | head -c ${l} | xargs
}
